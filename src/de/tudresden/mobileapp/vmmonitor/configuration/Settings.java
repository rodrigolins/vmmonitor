package de.tudresden.mobileapp.vmmonitor.configuration;

public class Settings
{
	public static final String GCM_SENDER = "910238809008";

	public static final String API_URL = "http://192.168.56.1:8001/gcm/v1/device/";
	
	public static final String MONITOR_URL = "http://192.168.56.1:8001/api/";
	
	public static final String TAG = "VMMonitor";
	
	public static final String SHA1_FINGERPRINT = "F4:25:99:C9:FE:14:49:E1:3A:B7:1E:4F:71:A6:C8:3E:57:BC:C2:B4";
	
	public static final String API_KEY = "AIzaSyAlBgOhC8HwrO0Y6kwNm06qcCF6zpcx_xM";
	
	public static final String EXTRA_MESSAGE = "message";
	
    public static final String PROPERTY_REG_ID = "registration_id";
    
    public static final String PROPERTY_APP_VERSION = "appVersion";
    
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    
}

package de.tudresden.mobileapp.vmmonitor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class RunScript extends Activity {
	Context context = this;
     static  String result;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_run_script);
		TextView tv = (TextView) findViewById(R.id.scrDesc);
		final String data = getIntent().getStringExtra("keyName");
		final String params[] = data.split("_");
		
		tv.setText(params[0]);
		// HttpPost httprun = new HttpPost("http://192.168.56.1/")

		Button b = (Button) findViewById(R.id.RunScript);
		b.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				final Thread th = new Thread(new Runnable() {
					@Override
					public void run() {

						String url = "http://192.168.56.1:8001/api/run/";

						HttpURLConnection httpcon = null;

						// String
						// data=" {"script_id":6,"server_id": 2,"status": <Anything>} ";
						StringBuilder str = new StringBuilder();
						str.append("{");
						str.append("\"script_id\":" + 1);
						str.append(",");
						str.append("\"server_id\":" + 1);
						str.append(",\"status\":0");
						str.append("}");

						// System.out.println("str===============>"+str.toString());

						// Connect
						try {
							httpcon = (HttpURLConnection) ((new URL(url)
									.openConnection()));
						} catch (MalformedURLException e4) {
							// TODO Auto-generated catch block
							e4.printStackTrace();
						} catch (IOException e4) {
							// TODO Auto-generated catch block
							e4.printStackTrace();
						}
						httpcon.setDoOutput(true);
						httpcon.setRequestProperty("Content-Type",
								"application/json");
						httpcon.setRequestProperty("Accept", "application/json");
						try {
							httpcon.setRequestMethod("POST");
						} catch (ProtocolException e3) {
							// TODO Auto-generated catch block
							e3.printStackTrace();
						}
						try {
							httpcon.connect();
						} catch (IOException e3) {
							// TODO Auto-generated catch block
							e3.printStackTrace();
						}

						// Write
						OutputStream os = null;
						try {
							os = httpcon.getOutputStream();
						} catch (IOException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
						BufferedWriter writer = null;
						try {
							writer = new BufferedWriter(new OutputStreamWriter(
									os, "UTF-8"));
						} catch (UnsupportedEncodingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							writer.write(str.toString());
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							writer.close();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							os.close();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						// Read
						BufferedReader br = null;
						try {
							br = new BufferedReader(new InputStreamReader(
									httpcon.getInputStream(), "UTF-8"));
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						try {
							Thread.sleep(30);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						FetchServerData fs = new FetchServerData();
						
						try {
							result = fs.getScriptResult(
									Integer.valueOf(params[1]),
									Integer.valueOf(params[2]));
							monitorDataDB db = new monitorDataDB(context);
							db.createResTableStoreData(Integer.valueOf(params[1]),result);
							//Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
//							alert(result);
//							System.out.println("dis pl is===>" + result);
						} catch (NumberFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				});
				
				th.start();
				monitorDataDB db = new monitorDataDB(context);
				alert(db.getScriptDataAndDelete(Integer.valueOf(params[1])));
//				System.out.println("dsply is===>" + result);
//				System.out.flush();
//				AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
//				
//				builder1.setMessage(result);
//
//				builder1.setCancelable(true);
//				builder1.setPositiveButton("Ok",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog, int id) {
//								dialog.cancel();
//							}
//						});
//
//				AlertDialog alert11 = builder1.create();
//				alert11.show();
		}

		});

	}

	void alert(String result ) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
		builder1.setMessage(result);

		builder1.setCancelable(true);
		builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		AlertDialog alert11 = builder1.create();
		alert11.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.RunScript, menu);
		return true;
	}

}

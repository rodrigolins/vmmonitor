package de.tudresden.mobileapp.vmmonitor;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Property;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class VMProperty extends Activity {

	String[] properties = new String[] { "Cpu Usage :   ", "Memory Usage  :",
			"SSD Usage : ", "Network Usage : ", "Num Processes: " };
	int i = 0;
	Context obj = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vmproperty);

		// mainListView.notifyAll();
		 final String i = getIntent().getStringExtra("keyName");
		 FetchServerData fs = new FetchServerData();
		 String data = fs.getVmProperties(Integer.valueOf(i));
		 ParseJSON js = new ParseJSON();
		 Object[] property = null;
		 try {
			System.out.println("data is====>"+js.parseVmProperties(data).toString());
			property = js.parseVmProperties(data).toArray();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		properties[0] = "Cpu Usage :" + property[0];
		properties[1] = "Memory Usage :" + property[1];
		properties[2] = "Disk  Usage :" + property[2];
		properties[3] = "Network Usage :" + property[3];
		properties[4] = "num Processes :" + property[4];
		
		final ListView mainListView = (ListView) findViewById(R.id.Proplist);
		final ArrayList<String> planetList = new ArrayList<String>();
		planetList.addAll(Arrays.asList(properties));
		ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(obj,
				R.layout.simplerow, planetList);
		mainListView.setAdapter(listAdapter);
		
		

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.vmproperty, menu);
		return true;
	}

	private static class getVmPropertyIO extends
			AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {

			// GetPropertySocketIO sock = new GetPropertySocketIO();
			// sock.testServer();

			try {
				System.out.println("before start");
				GetVmProperties ws = new GetVmProperties(new URI(
						"ws://echo.websocket.org/"));

				ws.start();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch bloc
				e.printStackTrace();
			}

			System.out.flush();

			// System.out.println("doInBackground....");

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// myResult = result;
			// System.out.println(result);
			// if (results!=null) {
			// EditText et = (EditText)findViewById(R.id.my_edit);
			// et.setText(results);
			// }
			// Button b = (Button)findViewById(R.id.my_button);
			// b.setClickable(true);
		}
	}
}

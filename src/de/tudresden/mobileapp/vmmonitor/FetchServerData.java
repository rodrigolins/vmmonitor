package de.tudresden.mobileapp.vmmonitor;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;

import android.os.AsyncTask;
import de.tudresden.mobileapp.vmmonitor.configuration.Settings;

public class FetchServerData
{
	private static String myResult;
	

	public String getAllServers() {
		AsyncTask<String, Void, String> a;
		String serverURL = Settings.MONITOR_URL + "server";
		FetchServerData.LongRunningGetIO task = new FetchServerData.LongRunningGetIO();
		a = task.execute(new String[] { serverURL });

		// try {
		// System.out.println("result is===>"+a.get());
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (ExecutionException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		try {
			return a.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String getScriptInfo(String path) {

		AsyncTask<String, Void, String> a;
		String serverURL = "http://192.168.56.1:8001" + path;
		FetchServerData.LongRunningGetIO task = new FetchServerData.LongRunningGetIO();
		a = task.execute(new String[] { serverURL });

		try {
			System.out.println("result is===>" + a.get());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			return a.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String getScripts() {

		String serverURL = Settings.MONITOR_URL + "script";
		AsyncTask<String, Void, String> a;

		FetchServerData.LongRunningGetIO task = new FetchServerData.LongRunningGetIO();
		a = task.execute(new String[] { serverURL });

		try {
			System.out.println("result is===>" + a.get());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			return a.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static class LongRunningGetIO extends
			AsyncTask<String, Void, String> {

		protected String getJSONResult(HttpEntity entity)
				throws IllegalStateException, IOException {
			InputStream in = entity.getContent();
			StringBuffer out = new StringBuffer();
			int n = 1;
			while (n > 0) {
				byte[] b = new byte[4096];
				n = in.read(b);
				if (n > 0)
					out.append(new String(b, 0, n));
			}
			return out.toString();
		}

		@Override
		protected String doInBackground(String... params) {
			System.out.println(Arrays.toString(params));
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			HttpGet httpGet = new HttpGet(params[0]);
			String text = null;
			try {
				HttpResponse response = httpClient.execute(httpGet,
						localContext);
				HttpEntity entity = response.getEntity();
				text = getJSONResult(entity);
			} catch (Exception e) {
				return e.getLocalizedMessage();
			}
			System.out.println("doInBackground....");

			return text;
		}

		
		
		@Override
		protected void onPostExecute(String result) {
			myResult = result;
			// System.out.println(result);
			// if (results!=null) {
			// EditText et = (EditText)findViewById(R.id.my_edit);
			// et.setText(results);
			// }
			// Button b = (Button)findViewById(R.id.my_button);
			// b.setClickable(true);
		}
	}

	public String getVmProperties(Integer valueOf) {
		AsyncTask<String, Void, String> a;
		String serverURL = "http://192.168.56.1:8001/api/statistics/?server_id=" +valueOf;
		FetchServerData.LongRunningGetIO task = new FetchServerData.LongRunningGetIO();
		a = task.execute(new String[] { serverURL });

		try {
			System.out.println("result is===>" + a.get());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			return a.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	public String getScriptResult(int i, int j) throws JSONException  {
		AsyncTask<String, Void, String> a;
		 String  scrresult = null ;
		String serverURL = "http://192.168.56.1:8001/api/run/?server_id=" +i+"&script_id="+j;
		FetchServerData.LongRunningGetIO task = new FetchServerData.LongRunningGetIO();
		a = task.execute(new String[] { serverURL });

		try {
		//	System.out.println("result is===>" + a.get());
			ParseJSON js = new ParseJSON();
			scrresult = js.parseScriptResult(a.get());
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("result is===>" +scrresult);
		return scrresult;
		
	}
	
	
}
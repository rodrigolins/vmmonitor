package de.tudresden.mobileapp.vmmonitor;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.tudresden.mobileapp.vmmonitor.businessobjects.Script;
import de.tudresden.mobileapp.vmmonitor.businessobjects.Server;

public class ParseJSON {

	// Method that will parse the JSON file and will return a JSONObject
	public List<Server> parseJSONData(String json) throws JSONException {
		String JSONString = null;
		JSONObject JSONObject = null;

		JSONObject JSONscriptInfoObject = null;
		// byte[] bytes = new byte[json.]
		try {
			JSONString = new String(json.getBytes(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JSONObject = new JSONObject(JSONString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONArray a = JSONObject.getJSONArray("objects");
		int arrSize = a.length();
		JSONObject o;
		// arrSize = 1;
		List<Server> serlist = new ArrayList<Server>();
		Server ser = null;
		for (int i = 0; i < arrSize; ++i) {
			o = a.getJSONObject(i);
			ser = new Server();
			ser.setId(o.getLong("id"));
			ser.setIp(o.getString("ip"));
			ser.setLocation(o.getString("location"));

			JSONArray scripts = o.getJSONArray("scripts");
			// JSONArray sc = JSONScriptsObject.getJSONArray("objects");
			int scArSize = scripts.length();
			List<Script> scl = new ArrayList<Script>();

			for (int j = 0; j < scArSize; ++j) {
				Script scpt = new Script();

				System.out.println("sc=====>" + scripts.getString(j));
				String scriptinfo = null;
				try {
					scriptinfo = new String(new FetchServerData()
							.getScriptInfo(scripts.getString(j)).getBytes(),
							"UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					JSONscriptInfoObject = new JSONObject(scriptinfo);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				scpt.setFile(JSONscriptInfoObject.getString("file"));
				scpt.setId(JSONscriptInfoObject.getLong("id"));
				scpt.setName(JSONscriptInfoObject.getString("name"));

				scl.add(scpt);

				// System.out.println("file=====>"
				// + JSONscriptInfoObject.getString("file"));
				// System.out.println("id=====>"
				// + JSONscriptInfoObject.getLong("id"));
				// System.out.println("name=====>"
				// + JSONscriptInfoObject.getString("name"));
			}

			ser.setScripts(scl);
			serlist.add(ser);

		}

		return serlist;

	}
	
	public ArrayList<String> parseVmProperties(String data) throws JSONException{
		
		ArrayList<String>res = new ArrayList<String>();
		String JSONString = null;
		JSONObject JSONObject = null;

		JSONObject JSONscriptInfoObject = null;
		// byte[] bytes = new byte[json.]
		try {
			JSONString = new String(data.getBytes(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JSONObject = new JSONObject(JSONString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONArray a = null;
		try {
			a = JSONObject.getJSONArray("objects");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int arrSize = a.length();
		JSONObject o = null;
		
			try {
				o = a.getJSONObject(arrSize-1);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			res.add(String.valueOf(o.getInt("cpu")));
			res.add(o.getString("memory"));
			res.add(o.getString("network"));
			res.add(o.getString("disk"));
			res.add(String.valueOf(o.getInt("processes")));
			return res;
			
			
	}

public String parseScriptResult(String data) throws JSONException{
		
		//ArrayList<String>res = new ArrayList<String>();
		String JSONString = null;
		JSONObject JSONObject = null;

		JSONObject JSONscriptInfoObject = null;
		// byte[] bytes = new byte[json.]
		try {
			JSONString = new String(data.getBytes(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JSONObject = new JSONObject(JSONString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONArray a = null;
		try {
			a = JSONObject.getJSONArray("objects");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JSONObject o = null;
		
			try {
				o = a.getJSONObject(0);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//System.out.println("json result is ===>"+o.toString().substring(0,20));
		
			return	o.toString();//
	}
	
}

package de.tudresden.mobileapp.vmmonitor;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import de.tudresden.mobileapp.vmmonitor.configuration.Settings;

public class RegistrationActivity extends Activity
{
	Button registrationBtn;
	TextView registrationStatus;
	TextView registrationId;
	String regid = "";
	Context context;
	GoogleCloudMessaging gcm;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.registration);

		context = getApplicationContext();
		
		gcm = GoogleCloudMessaging.getInstance(this);

		registrationBtn = (Button) findViewById(R.id.registration);
		registrationStatus = (TextView) findViewById(R.id.current_status);
		registrationId = (TextView) findViewById(R.id.current_registration_id);
		registrationBtn.setOnClickListener(buttonListener);

		regid = getRegistrationId(context);

		if (regid.equals("")) {
			registrationStatus.setText("Unregistered");
			registrationId.setText("Not registed");
			registrationBtn.setText("Register");
		} else {
			registrationStatus.setText("Registered");
			registrationId.setText(regid);
			registrationBtn.setText("Unregister");
		}
	}

	private final OnClickListener buttonListener = new OnClickListener()
	{
		public void onClick(View v)
		{
			Log.i(Settings.TAG, "Called click listener on Registration Activity");
			if (regid.equals(""))
			{
				 registerInBackground();
			}
			else
			{
				 unregisterInBackground();
			}

			Thread welcomeThread = new Thread()
			{
				@Override
				public void run()
				{
					try
					{
						sleep(3000);
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
					finally
					{
						finish();
						startActivity(getIntent());
					}
				}
			};
			welcomeThread.start();
		}
	};

	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGcmPreferences(context);
		String registrationId = prefs.getString(Settings.PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(Settings.TAG, "Registration not found.");
			return "";
		}
		return registrationId;
	}

	private SharedPreferences getGcmPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(MainActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	private void registerInBackground() {
		Log.i(Settings.TAG, "Registring cellphone");
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					regid = gcm.register(Settings.GCM_SENDER);
					Log.i(Settings.TAG, "Cellphone registered with cellphone id: " + regid);
					msg = "Device registered, registration ID=" + regid;
					System.out.println("MSG:" + msg);
					sendRegistrationIdToBackend();
					storeRegistrationId(context, regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
			}
		}.execute(null, null, null);
	}
	
    private void unregisterInBackground() {
    	Log.i(Settings.TAG, "Unregistering cellphone");
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    gcm.unregister();
                    System.out.println("MSG: Device unregistered!!!");
                    regid = "";
                    sendUnregistrationIdToBackend();
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }
    
    private void sendRegistrationIdToBackend() {
    	Log.i(Settings.TAG, "Registering device");
        String deviceID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
        Device.register(deviceID, regid);
        Log.i(Settings.TAG, "Device registered");
    }
    
    private void sendUnregistrationIdToBackend() {
    	Log.i(Settings.TAG, "Unregistering device");
        String deviceID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
        Device.unregister(deviceID);
        Log.i(Settings.TAG, "Device unregistered");
    }
    
    private void storeRegistrationId(Context context, String regId)
    {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(Settings.TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Settings.PROPERTY_REG_ID, regId);
        editor.putInt(Settings.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }
    private static int getAppVersion(Context context)
    {
        try
        {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        }
        catch (NameNotFoundException e)
        {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}

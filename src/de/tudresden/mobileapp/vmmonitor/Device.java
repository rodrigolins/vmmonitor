package de.tudresden.mobileapp.vmmonitor;

import org.apache.http.message.BasicNameValuePair;

import de.tudresden.mobileapp.vmmonitor.configuration.Settings;
import de.tudresden.mobileapp.vmmonitor.restapi.RestClient;

public class Device
{
    public static void register(String deviceID, String registrationId) {
        String url = Settings.API_URL + "register/";

        RestClient client = new RestClient(url);

        client.addPostParam(new BasicNameValuePair("dev_id", deviceID));
        client.addPostParam(new BasicNameValuePair("reg_id", registrationId));
        System.out.println(client.toString());
        client.execute();
    }

    public static void unregister(String deviceID) {
        String url = Settings.API_URL + "unregister/";

        RestClient client = new RestClient(url);
        client.addPostParam(new BasicNameValuePair("dev_id", deviceID));
        client.execute();
    }
}